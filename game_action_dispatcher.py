import bot_keyboards
import game_tools
import localization
import json
import models
import random
import logging
import items.dispatcher
import objects.dispatcher
import spells.dispatcher


ACTION_DISPATCHER_DICT = dict()


def process_action(character, communication_callback):
    planned_action = character.planned_action
    if planned_action == "idle":
        communication_callback(text=localization.format_safe(character.game.host, "action_idle", name=character.name))
        character.planned_action = None
        character.save()
        return
    logging.info(f"Executing action {planned_action} for {character.name}")
    action = planned_action.split()
    if action[0] not in ACTION_DISPATCHER_DICT:
        logging.error(f"{character.name}'s action not in action dispatcher dict: {action[0]}")
        communication_callback(text=f"ACTION DISPATCHER ERROR\\: invalid action {action[0]} for {character.name}")
        return
    result = ACTION_DISPATCHER_DICT[action[0]](character, action, character.user, character.game, communication_callback)

    character.planned_action = None
    character.save()


def ascend(character, action, user, game, communication_callback):
    if game_tools.is_in_battle(character):
        return localization.format_safe(user, "action_ascend_blocked", name=character.name)
    floor = character.floor
    if floor.height == 100:
        character.game.active = False
        character.game.save()
        communication_callback(
            localization.format_safe(user, "action_ascend_top", name=character.name),
            reply_markup=bot_keyboards.get_restart_keyboard(user, character)
        )
        return
    next_floor = models.Floor.get(game=character.game, height=floor.height+1)

    character.floor = next_floor
    character.save()

    communication_callback(
        text=f"{localization.format_safe(user, 'action_ascend', name=character.name, floor=character.floor.height)}"
    )


ACTION_DISPATCHER_DICT["ascend"] = ascend


def descend(character, action, user, game, communication_callback):
    if game_tools.is_in_battle(character):
        roll_result = communication_callback(roll=True, text=localization.format_safe(
            user,
            "action_descend_trying",
            name=character.name,
            floor=character.floor.height-1
        ))
        if roll_result < 5:
            communication_callback(text=localization.localize(user, "action_descend_failed"))
            return
        descend_message = localization.localize(user, 'action_descend_run_success')
    else:
        descend_message = localization.format_safe(user,
                                                   'action_descend',
                                                   name=character.name,
                                                   floor=character.floor.height-1)

    floor = character.floor
    if floor.height == 1:
        communication_callback(
            localization.format_safe(user, "descend_bottom", name=character.name)
        )
        return

    next_floor = models.Floor.get(game=character.game, height=floor.height-1)

    character.floor = next_floor
    character.save()

    communication_callback(
        f"{descend_message}"
    )


ACTION_DISPATCHER_DICT["descend"] = descend


@game_tools.add_data
def take(character_inventory, entity_data, character, action, user, game, communication_callback):
    item_id = action[1]
    if item_id not in entity_data['items']:
        communication_callback(text=localization.format_safe(user, "inventory_no_such_item_on_floor"))
        return

    item = entity_data['items'][item_id]

    def process_removal():
        del entity_data['items'][item_id]

        with models.db.atomic():
            character.inventory_data = json.dumps(character_inventory)
            character.floor.entity_data = json.dumps(entity_data)

            character.save()
            character.floor.save()

    if "type" in item and item['type'] == "WEAPON":
        if character_inventory['weapon'] is not None:
            game_tools.register_item(entity_data, character_inventory['weapon'])
        character_inventory['weapon'] = item
        process_removal()
        communication_callback(
            text=localization.format_safe(user, "action_take_equipped", name=character.name, item=items.dispatcher.describe_item(user, item))
        )
    else:
        if len(character_inventory['items']) >= 5:
            communication_callback(text=localization.localize(user, "inventory_no_free_slots"))
            return

        character_inventory["items"].append(item)
        process_removal()
        communication_callback(
            text=localization.format_safe(user, "action_take", name=character.name, item=items.dispatcher.describe_item(user, item))
        )


ACTION_DISPATCHER_DICT["take"] = take


@game_tools.add_data
def drop(character_inventory, entity_data, character, action, user, game, communication_callback):
    item_id = int(action[1])
    #
    # if len(character_inventory['items']) <= item_id:
    #     communication_callback(localization.localize(user, "inventory_no_such_item"))
    #     return

    item = character_inventory['items'][item_id]
    character_inventory["items"].remove(item)
    entity_data = json.loads(character.floor.entity_data)
    game_tools.register_item(entity_data, item)

    with models.db.atomic():
        character.inventory_data = json.dumps(character_inventory)
        character.floor.entity_data = json.dumps(entity_data)

        character.save()
        character.floor.save()

    communication_callback(
        text=localization.format_safe(user, "action_drop", name=character.name, item=items.dispatcher.describe_item(user, item))
    )


ACTION_DISPATCHER_DICT["drop"] = drop


@game_tools.add_data
def attack(character_inventory, entity_data, character, action, user, game, communication_callback):
    if len(entity_data['enemies']) == 0:
        communication_callback(text=localization.localize(user, "action_attack_no_enemies"))
        return

    if character_inventory["weapon"] is None:
        communication_callback(text=localization.localize(user, "action_attack_no_weapon"))
        return

    if len(action) < 2 or action[1] not in entity_data['enemies']:
        target_id, target = random.choice(list(entity_data['enemies'].items()))
    else:
        target = entity_data['enemies'][action[1]]
        target_id = action[1]

    communication_callback(text=localization.format_safe(user, "battle_attack", name=character.name, target=game_tools.describe_enemy(user, target)))

    combo = game_tools.process_attack(user, communication_callback)

    if combo > 0:
        damage = character_inventory["weapon"]["damage"]*2**(combo-1)
        communication_callback(
            text=localization.format_safe(user, "battle_taking_damage", target=game_tools.describe_enemy(user, target),
                                          damage=damage))

        game_tools.damage_enemy(entity_data, user, target, target_id, damage, communication_callback)

        character.floor.entity_data = json.dumps(entity_data)
        character.floor.save()


ACTION_DISPATCHER_DICT["attack"] = attack


@game_tools.add_data
def use(character_inventory, entity_data, character, action, user, game, communication_callback):
    item_id = int(action[1])
    item = character_inventory['items'][item_id]

    target_id = None
    if len(action) == 3:
        target_id = action[2]

    items.dispatcher.use(
        character_inventory,
        entity_data,
        character,
        user,
        game,
        item,
        target_id,
        communication_callback
    )


ACTION_DISPATCHER_DICT["use"] = use


@game_tools.add_data
def cast(character_inventory, entity_data, character, action, user, game, communication_callback):
    spell_name = action[1]

    spell_power = None
    if len(action) >= 3:
        spell_power = int(action[2])
    target_id = None
    if len(action) == 4:
        target_id = action[3]

    spells.dispatcher.cast(
        character_inventory,
        entity_data,
        character,
        user,
        game,
        spell_name,
        spell_power,
        target_id,
        communication_callback
    )


ACTION_DISPATCHER_DICT["cast"] = cast


@game_tools.add_data
def interact(character_inventory, entity_data, character, action, user, game, communication_callback):
    object_id = action[1]
    obj = entity_data['objects'][object_id]
    objects.dispatcher.interact(character_inventory, entity_data, character, user, game, obj, communication_callback)


ACTION_DISPATCHER_DICT["interact"] = interact
