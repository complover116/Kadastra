from random import Random

import game_tools
import items.dispatcher
import localization
import generator
from . import constants

REQUIRED_TARGET = None
HAS_RARITIES = True


def plan_use(inventory, entity_data, character, query, user, game, item, target):
    if len(entity_data['enemies']) == 0:
        return localization.localize(user, "action_attack_no_enemies")
    return constants.PLAN_OK


def use(inventory, entity_data, character, user, game, item, target, target_id, communication_callback):
    communication_callback(text=localization.format_safe(
        user,
        "item_magic_grenade_use",
        item=items.dispatcher.describe_item(user, item),
        damage=item['damage']
    ))

    for enemy_id, enemy in list(entity_data['enemies'].items()):
        game_tools.damage_enemy(entity_data, user, enemy, enemy_id, item['damage'], communication_callback)
    inventory['items'].remove(item)


def describe_postfix(user, item):
    return f" \\[{item['damage']} {localization.localize(user, 'damage')}\\]"


def generate(random_instance: Random, rarity, level):
    base_damage = game_tools.ITEM_DATA["item_magic_grenade"]["base_damage"]*generator.exponential_level_scale(level)
    multiplier = generator.random_float_between(
        random_instance,
        game_tools.ITEM_DATA['rarities'][rarity]['damage_mul_min'],
        game_tools.ITEM_DATA['rarities'][rarity]['damage_mul_max']
    )
    grenade = {
        "name": "item_magic_grenade",
        "rarity": rarity,
        "damage": int(base_damage*multiplier)
    }
    return grenade
