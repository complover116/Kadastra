import random

import game_tools
import items.dispatcher
import localization
import generator
from . import constants

REQUIRED_TARGET = None
HAS_RARITIES = True


def plan_use(inventory, entity_data, character, query, user, game, item, target):
    if len(entity_data['enemies']) == 0:
        return localization.localize(user, "action_attack_no_enemies")
    return constants.PLAN_OK


def use(inventory, entity_data, character, user, game, item, target, target_id, communication_callback):
    communication_callback(text=localization.format_safe(
        user,
        "item_stun_grenade_use",
        item=items.dispatcher.describe_item(user, item)
    ))

    for enemy_id, enemy in list(entity_data['enemies'].items()):
        game_tools.apply_status_effect(
            entity_data,
            user,
            enemy,
            {
                "type": "stun",
                "duration": int(generator.random_float_between(
                    random.Random(),
                    game_tools.ITEM_DATA['rarities'][item['rarity']]['nonscaling_mul_min'],
                    game_tools.ITEM_DATA['rarities'][item['rarity']]['nonscaling_mul_max']
                ))
            },
            communication_callback
        )
    inventory['items'].remove(item)


def describe_postfix(user, item):
    min_stun = int(game_tools.ITEM_DATA['rarities'][item['rarity']]['nonscaling_mul_min'])
    max_stun = int(game_tools.ITEM_DATA['rarities'][item['rarity']]['nonscaling_mul_max'])
    return f" \\[{min_stun}\\-{max_stun} {localization.localize(user, 'turns')}\\]"


def generate(random_instance: random.Random, rarity, level):
    grenade = {
        "name": "item_stun_grenade",
        "rarity": rarity
    }
    return grenade
