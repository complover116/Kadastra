import game_tools
import items.dispatcher
import localization
import generator
import math
from . import constants

REQUIRED_TARGET = None
HAS_RARITIES = True


def plan_use(inventory, entity_data, character, query, user, game, item, target):
    return constants.PLAN_OK


def use(inventory, entity_data, character, user, game, item, target, target_id, communication_callback):
    item["uses"] -= 1
    character.health += item["healing"]
    if character.health > character.HP:
        character.health = character.HP
    communication_callback(text=localization.format_safe(
        user,
        "effect_restore_health",
        name=character.name,
        healing=item["healing"],
        health=character.health,
        hp=character.HP
    ))
    if item["uses"] == 0:
        communication_callback(text=localization.format_safe(
            user,
            "action_use_potion_empty",
            name=character.name,
            potion=items.dispatcher.describe_item(user, item)
        ))
        inventory['items'].remove(item)


def describe_postfix(user, item):
    return f" \\[\\+{item['healing']} HP \\| {item['uses']} {localization.localize(user, 'uses')}\\]"


def generate(random_instance, rarity, level):
    base_healing = game_tools.ITEM_DATA["item_health_potion"]["base_healing"]
    base_uses = game_tools.ITEM_DATA["item_health_potion"]["base_uses"]
    multiplier = generator.random_float_between(
        random_instance,
        game_tools.ITEM_DATA['rarities'][rarity]['damage_mul_min'],
        game_tools.ITEM_DATA['rarities'][rarity]['damage_mul_max']
    )
    scaling = generator.exponential_level_scale(level)
    uses_multiplier = generator.random_float_between(
        random_instance,
        game_tools.ITEM_DATA['rarities'][rarity]['uses_mul_min'],
        game_tools.ITEM_DATA['rarities'][rarity]['uses_mul_max']
    )
    potion = {
        "name": "item_health_potion",
        "rarity": rarity,
        "healing": int(base_healing * multiplier * scaling),
        "uses": int(math.ceil(base_uses * uses_multiplier))
    }
    return potion
