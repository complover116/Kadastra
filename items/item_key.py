import game_tools
import items.dispatcher
import localization
import objects.dispatcher
from . import constants

REQUIRED_TARGET = None
HAS_RARITIES = True


def plan_use(inventory, entity_data, character, query, user, game, item, target):
    chest_exists, top_chest, target_chest, target_chest_id = get_chests(entity_data, item)
    if target_chest is not None:
        return constants.PLAN_OK
    else:
        return error_message(chest_exists, user, character, top_chest)


def get_chests(entity_data, item):
    chest_exists = False
    top_chest = None
    target_chest = None
    target_chest_id = None
    for obj_id, obj in entity_data["objects"].items():
        if obj["type"] == "CHEST":
            chest_exists = True
            if target_chest is None or game_tools.ITEM_DATA["rarity_order"].index(obj['rarity']) > game_tools.ITEM_DATA["rarity_order"].index(target_chest['rarity']):
                top_chest = obj
                if game_tools.ITEM_DATA['rarity_order'].index(obj['rarity']) > game_tools.ITEM_DATA['rarity_order'].index(item['rarity']):
                    continue
                target_chest = obj
                target_chest_id = obj_id
    return chest_exists, top_chest, target_chest, target_chest_id


def error_message(chest_exists, user, character, top_chest):
    if chest_exists:
        return localization.format_safe(user, "action_use_chest_too_rare",
                                        name=character.name,
                                        chest=objects.dispatcher.describe_object(user, top_chest),
                                        rarity=localization.localize(user, top_chest['rarity'])
                                        )
    else:
        return localization.format_safe(user, "inventory_no_chest")


def use(inventory, entity_data, character, user, game, item, target, target_id, communication_callback):
    chest_exists, top_chest, target_chest, target_chest_id = get_chests(entity_data, item)
    if target_chest is not None:
        inventory['items'].remove(item)
        game_tools.register_item(entity_data, target_chest['item'])
        del entity_data['objects'][target_chest_id]
        communication_callback(
            text=localization.format_safe(
                user,
                "action_use_chest_opened",
                name=character.name,
                chest=objects.dispatcher.describe_object(user, target_chest),
                item=items.dispatcher.describe_item(user, target_chest['item'])
            )
        )
    else:
        communication_callback(error_message(chest_exists, user, character, top_chest))
        return


def describe_postfix(user, item):
    return ""


def generate(random_instance, rarity, level):
    return {
        "name": "item_key",
        "rarity": rarity
    }
