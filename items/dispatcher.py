from . import constants
import items.item_key
import items.item_health_potion
import items.item_rod_of_lightning
import items.item_magic_grenade
import items.item_fire_staff
import items.item_stun_grenade
import items.item_mana_potion
import items.item_spell_book
import game_tools
import localization
import bot_keyboards
import json
import models

ITEM_TYPES = dict()
ITEM_TYPES['item_key'] = items.item_key
ITEM_TYPES['item_health_potion'] = items.item_health_potion
ITEM_TYPES['item_mana_potion'] = items.item_mana_potion
ITEM_TYPES['item_rod_of_lightning'] = items.item_rod_of_lightning
ITEM_TYPES['item_magic_grenade'] = items.item_magic_grenade
ITEM_TYPES['item_fire_staff'] = items.item_fire_staff
ITEM_TYPES['item_stun_grenade'] = items.item_stun_grenade
ITEM_TYPES['item_spell_book'] = items.item_spell_book


def plan_use(inventory, entity_data, character, query, user, game, item_id, item, target_id):
    assert item['name'] in ITEM_TYPES
    item_type = ITEM_TYPES[item['name']]
    if item_type.REQUIRED_TARGET is not None:
        if target_id is None:
            if item_type.REQUIRED_TARGET == constants.TARGET_ENEMY:
                query.edit_message_text(
                    text=localization.format_safe(user, "plan_use_target_select", item=describe_item(user, item)),
                    parse_mode="MarkdownV2",
                    reply_markup=bot_keyboards.get_inline_keyboard_enemy_target(user, character, f"use {item_id}")
                )
                return None
            else:
                assert False  # Invalid target type

        target = _get_target_from_id(inventory, entity_data, item_type.REQUIRED_TARGET, target_id)
    else:
        target = None

    result = item_type.plan_use(inventory, entity_data, character, query, user, game, item, target)
    if result == constants.PLAN_OK:
        if target is not None:
            return localization.format_safe(user, "plan_use_target", name=character.name,
                                            item=describe_item(game.host, item), target=game_tools.describe_enemy(user, target))
        else:
            return localization.format_safe(user, "plan_use", name=character.name, item=describe_item(game.host, item))
    else:
        return False, result


def use(inventory, entity_data, character, user, game, item, target_id, communication_callback):
    assert item['name'] in ITEM_TYPES
    item_type = ITEM_TYPES[item['name']]
    if item_type.REQUIRED_TARGET is not None:
        assert target_id is not None
        target = _get_target_from_id(inventory, entity_data, item_type.REQUIRED_TARGET, target_id)
        if target is None:
            communication_callback(text=localization.format_safe(user, "action_use_no_target"))
        else:
            # TODO: Support describing any target
            communication_callback(
                text=localization.format_safe(user, "action_use_target", name=character.name,
                                              item=describe_item(game.host, item),
                                              target=game_tools.describe_enemy(game.host, target))
            )
    else:
        communication_callback(
            text=localization.format_safe(user, "action_use", name=character.name,
                                          item=describe_item(game.host, item))
        )
        target = None

    result = item_type.use(
        inventory,
        entity_data,
        character,
        user,
        game,
        item,
        target,
        target_id,
        communication_callback
    )
    with models.db.atomic():
        character.inventory_data = json.dumps(inventory)
        character.save()
        character.floor.entity_data = json.dumps(entity_data)
        character.floor.save()


def _get_target_from_id(inventory, entity_data, target_type, target_id):
    if target_type == constants.TARGET_ENEMY:
        return entity_data['enemies'][target_id]
    else:
        assert False  # Invalid target type


def describe_item(user, item):
    main_name = localization.localize(user, item["name"])

    if "type" in item and item['type'] == "WEAPON":
        data_postfix = f" \\[{item['damage']} {localization.localize(user, 'damage')}\\]"
    else:
        assert item['name'] in ITEM_TYPES
        item_type = ITEM_TYPES[item['name']]
        data_postfix = item_type.describe_postfix(user, item)

    if "type" in item or ITEM_TYPES[item['name']].HAS_RARITIES:
        return game_tools.describe_add_rarity(user, f"{main_name}{data_postfix}", item['rarity'])
    else:
        return f"{main_name}{data_postfix}"


def generate_item(random_instance, item_type_name, rarity, level):
    assert item_type_name in ITEM_TYPES
    item_type = ITEM_TYPES[item_type_name]
    return item_type.generate(random_instance, rarity, level)
