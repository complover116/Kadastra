from random import Random

import game_tools
import items.dispatcher
import localization
import generator
from . import constants

REQUIRED_TARGET = None
HAS_RARITIES = False


def plan_use(inventory, entity_data, character, query, user, game, item, target):
    if item['spell'] in inventory["spells"]:
        return localization.format_safe(user, "item_spell_book_plan_known", name=character.name)
    return constants.PLAN_OK


def use(inventory, entity_data, character, user, game, item, target, target_id, communication_callback):
    communication_callback(text=localization.format_safe(
        user,
        "item_spell_book_use",
        name=character.name,
        spell=localization.localize(user, item['spell'])
    ))
    inventory['spells'].append(item['spell'])
    inventory['items'].remove(item)


def describe_postfix(user, item):
    return f" \\[{localization.localize(user, item['spell'])}\\]"


def generate(random_instance: Random, rarity, level):
    raise Exception("Book of spells should never be generated randomly!")
