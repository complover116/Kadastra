import random

import game_tools
import items.dispatcher
import localization
import generator
import math
from . import constants

REQUIRED_TARGET = constants.TARGET_ENEMY
HAS_RARITIES = True


def plan_use(inventory, entity_data, character, query, user, game, item, target):
    return constants.PLAN_OK


def use(inventory, entity_data, character, user, game, item, target, target_id, communication_callback):
    assert item['uses'] > 0
    communication_callback(text=localization.format_safe(
        user,
        "item_fire_staff_use",
        target=game_tools.describe_enemy(user, target)
    ))
    game_tools.apply_status_effect(
        entity_data,
        user,
        target,
        {
            "type": "fire",
            "damage": item['damage'],
            "duration": random.randrange(2, 5)
        },
        communication_callback
    )
    item['uses'] -= 1
    if item['uses'] <= 0:
        communication_callback(text=localization.format_safe(
            user,
            "item_fire_staff_empty",
            item=items.dispatcher.describe_item(user, item),
            name=character.name
        ))
        inventory['items'].remove(item)


def describe_postfix(user, item):
    return f" \\[\\{item['damage']} {localization.localize(user, 'damage_fire')} \\| {item['uses']} {localization.localize(user, 'uses')}\\]"


def generate(random_instance, rarity, level):
    base_damage = game_tools.ITEM_DATA["item_fire_staff"]["base_damage"]*generator.exponential_level_scale(level)
    base_uses = game_tools.ITEM_DATA["item_fire_staff"]["base_uses"]
    multiplier = generator.random_float_between(
        random_instance,
        game_tools.ITEM_DATA['rarities'][rarity]['damage_mul_min'],
        game_tools.ITEM_DATA['rarities'][rarity]['damage_mul_max']
    )
    uses_multiplier = generator.random_float_between(
        random_instance,
        game_tools.ITEM_DATA['rarities'][rarity]['uses_mul_min'],
        game_tools.ITEM_DATA['rarities'][rarity]['uses_mul_max']
    )
    rod = {
        "name": "item_fire_staff",
        "rarity": rarity,
        "damage": int(base_damage*multiplier),
        "uses": int(math.ceil(base_uses * uses_multiplier))
    }
    return rod
