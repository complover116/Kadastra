import json
import re
import telegram

import game_tools
import items.dispatcher
import localization
import objects.dispatcher
from localization import format_safe


def get_inline_keyboard_okay(user, character):
    rows = (
        [[
            telegram.InlineKeyboardButton(
                text=format_safe(user, "keyboard_okay"),
                callback_data=f"describe"
            )
        ]]
    )
    reply_markup = telegram.InlineKeyboardMarkup(
        rows
    )

    return reply_markup


def get_inline_keyboard_take(user, character):
    entity_data = json.loads(character.floor.entity_data)
    rows = []
    for item_id, item in entity_data['items'].items():
        rows.append(
            [
                telegram.InlineKeyboardButton(
                    text=format_safe(user, "keyboard_take_item",
                                     item=markdown_cleanup.sub('', items.dispatcher.describe_item(user, item))),
                    callback_data=f"take {item_id}"
                )
            ]
        )

    rows.extend([
        [
            telegram.InlineKeyboardButton(
                text=format_safe(user, "keyboard_back"),
                callback_data=f"describe"
            )
        ]
    ])

    reply_markup = telegram.InlineKeyboardMarkup(
        rows
    )

    return reply_markup


def get_inline_keyboard_normal(user, character):
    entity_data = json.loads(character.floor.entity_data)
    rows = []

    if game_tools.is_in_battle(character):
        descend_text = format_safe(user, "keyboard_descend_run")
    else:
        descend_text = format_safe(user, "keyboard_descend")
        for obj_id, obj in entity_data['objects'].items():
            rows.append([
                telegram.InlineKeyboardButton(
                    text=format_safe(
                        user,
                        "keyboard_interact",
                        object=markdown_cleanup.sub("", objects.dispatcher.describe_object(user, obj))
                    ),
                    callback_data=f"interact {obj_id}"
                )
            ])

    move_buttons = [
        telegram.InlineKeyboardButton(
            text=descend_text,
            callback_data=f"descend"
        ),
        telegram.InlineKeyboardButton(
            text=format_safe(user, "keyboard_ascend"),
            callback_data=f"ascend"
        )
    ]

    battle_keys = [
        telegram.InlineKeyboardButton(
            text=format_safe(user, "keyboard_spellbook"),
            callback_data=f"spellbook"
        )
    ]
    if game_tools.is_in_battle(character):
        del move_buttons[1]
        battle_keys.insert(
            0,
            telegram.InlineKeyboardButton(
                text=format_safe(user, "keyboard_attack"),
                callback_data=f"attack"
            )
        )

    rows.append(battle_keys)

    inventory_buttons = []

    if entity_data['items']:
        inventory_buttons.append(
            telegram.InlineKeyboardButton(
                text=format_safe(user, "keyboard_take"),
                callback_data=f"take"
            )
        )

    inventory_buttons.append(
        telegram.InlineKeyboardButton(
            text=format_safe(user, "keyboard_inventory"),
            callback_data=f"inventory"
        )
    )

    rows.extend([
        inventory_buttons,
        move_buttons,
        [
            telegram.InlineKeyboardButton(
                text=format_safe(user, "keyboard_idle"),
                callback_data=f"idle"
            )
        ],
    ])
    reply_markup = telegram.InlineKeyboardMarkup(
        rows
    )

    return reply_markup


def get_restart_keyboard(user, character):
    reply_markup = telegram.ReplyKeyboardMarkup(
        [[f"/play {character.name}", f"/play {character.name} {character.game.seed}"]],
        resize_keyboard=True
    )

    return reply_markup


def get_inline_keyboard_enemy_target(user, character, action):
    entity_data = json.loads(character.floor.entity_data)
    rows = []
    for enemy_id, enemy in entity_data['enemies'].items():
        rows.append([
            telegram.InlineKeyboardButton(
                text=markdown_cleanup.sub("", game_tools.describe_enemy(user, enemy)),
                callback_data=f"{action} {enemy_id}"
            )
        ])

    rows.extend([
        [
            telegram.InlineKeyboardButton(
                text=format_safe(user, "keyboard_back"),
                callback_data=f"describe"
            )
        ]
    ])

    reply_markup = telegram.InlineKeyboardMarkup(
        rows
    )

    return reply_markup


def get_inline_keyboard_spell_power(user, character, action):
    rows = []

    if character.mana*10 >= character.MP:
        rows.append(
            [
                telegram.InlineKeyboardButton(
                    text=localization.localize(user, "spell_power_weak"),
                    callback_data=f"{action} 1"
                )
            ]
        )
    if character.mana*10//3 >= character.MP:
        rows.append(
            [
                telegram.InlineKeyboardButton(
                    text=localization.localize(user, "spell_power_normal"),
                    callback_data=f"{action} 2"
                )
            ]
        )
    if character.mana*2 >= character.MP:
        rows.append(
            [
                telegram.InlineKeyboardButton(
                    text=localization.localize(user, "spell_power_strong"),
                    callback_data=f"{action} 3"
                )
            ]
        )

    rows.append(
        [
            telegram.InlineKeyboardButton(
                text=format_safe(user, "keyboard_back"),
                callback_data=f"describe"
            )
        ]
    )

    reply_markup = telegram.InlineKeyboardMarkup(
        rows
    )

    return reply_markup


def get_inline_keyboard_inventory(user, character):
    inventory_data = json.loads(character.inventory_data)
    rows = []
    for item_id, item in enumerate(inventory_data['items']):
        rows.append(
            [
                telegram.InlineKeyboardButton(
                    text=format_safe(user, "keyboard_use",
                                     item=markdown_cleanup.sub('', items.dispatcher.describe_item(user, item))),
                    callback_data=f"use {item_id}"
                ),
                telegram.InlineKeyboardButton(
                    text=format_safe(user, "keyboard_drop"),
                    callback_data=f"drop {item_id}"
                )
            ]
        )

    rows.extend([
        [
            telegram.InlineKeyboardButton(
                text=format_safe(user, "keyboard_back"),
                callback_data=f"describe"
            )
        ]
    ])
    reply_markup = telegram.InlineKeyboardMarkup(
        rows
    )

    return reply_markup


def get_inline_keyboard_cast(user, character):
    inventory_data = json.loads(character.inventory_data)
    rows = []
    for spell_id, spell_name in enumerate(inventory_data['spells']):
        rows.append(
            [
                telegram.InlineKeyboardButton(
                    text=localization.localize(user, spell_name),
                    callback_data=f"cast {spell_name}"
                )
            ]
        )

    rows.extend([
        [
            telegram.InlineKeyboardButton(
                text=format_safe(user, "keyboard_back"),
                callback_data=f"describe"
            )
        ]
    ])
    reply_markup = telegram.InlineKeyboardMarkup(
        rows
    )

    return reply_markup


markdown_cleanup = re.compile(r"""[\\_*~]""")
