import game_tools
import models
import localization
import json
import random
import sys
import generator
import game_action_dispatcher
import bot
from game_tools import is_in_battle, describe_enemy, process_attack, register_item, damage_enemy
from objects.dispatcher import describe_object
from items.dispatcher import describe_item


def describe_floor(user, character):
    floor = character.floor
    if floor.height == 1:
        floor_description = localization.format_safe(user, "describe_floor_bottom")
    else:
        floor_description = localization.format_safe(user, "describe_floor_number", floor=floor.height)

    entities = json.loads(floor.entity_data)
    if len(entities['items']) == 0 and len(entities['enemies']) == 0 and len(entities['objects']) == 0:
        entities_description = localization.localize(user, "describe_floor_empty") + "\n"
    else:
        entities_description = ""
        if len(entities['items']) > 0:
            items_description = list()
            items_description.append(localization.localize(user, "describe_floor_items"))
            for item_id, item in enumerate(entities['items'].values()):
                items_description.append(f"{item_id + 1} \\- {describe_item(user, item)}")

            entities_description += "\n".join(items_description) + "\n"

        if len(entities['enemies']) > 0:
            enemies_description = list()
            enemies_description.append(localization.localize(user, "describe_enemies"))
            for enemy_id, enemy in enumerate(entities['enemies'].values()):
                status_effect_strings = [game_tools.describe_status_effect(user, effect) for effect in enemy["status_effects"]]
                status_effect_string = "\n   ".join([""] + status_effect_strings) if len(
                    status_effect_strings) > 0 else ""
                enemies_description.append(describe_enemy(user, enemy) + status_effect_string)

            entities_description += "\n".join(enemies_description) + "\n"

        if len(entities['objects']) > 0:
            objects_description = list()
            objects_description.append(localization.localize(user, "describe_objects"))
            for enemy_id, enemy in enumerate(entities['objects'].values()):
                objects_description.append(describe_object(user, enemy))

            entities_description += "\n".join(objects_description) + "\n"

    character_description = localization.format_safe(
        user,
        "character_status",
        health=character.health,
        hp=character.HP,
        mana=character.mana,
        mp=character.MP
    )

    if is_in_battle(character):
        exit_description = localization.localize(user, "describe_stairs_up_blocked")
    else:
        exit_description = localization.localize(user, "describe_stairs_up")

    return f"{floor_description}\n\n{entities_description}\n{character_description}\n{exit_description}"


def prepare_game(user, chat_id, seed=None):
    if seed is None:
        seed = random.randrange(sys.maxsize)
    game = models.Game.create(
        telegram_chat_id=chat_id,
        seed=seed,
        host=user
    )
    return game


def start_game(game):
    with models.db.atomic():
        generator.generate_game(game, len(models.PlayerCharacter.select().where(models.PlayerCharacter.game == game)))
        for character in models.PlayerCharacter.select().where(models.PlayerCharacter.game == game):
            spawn_character(character)
        game.started = True
    return game


def join_game(user, game, name):
    models.PlayerCharacter.create(
        game=game,
        name=name,
        inventory_data=json.dumps({
            "weapon": None,
            "items": [],
            "spells": []
        }),
        user=user,
    )


def spawn_character(character):
    character.floor = floor=models.Floor.get(game=character.game, height=1)
    character.save()
    return character


def check_and_do_turn(bot_instance, game, communication_callback):
    characters_playing = models.PlayerCharacter.select().where(models.PlayerCharacter.game == game)
    characters_ready_count = 0
    for character in characters_playing:
        if character.planned_action is not None:
            characters_ready_count += 1

    # communication_callback(text=localization.format_safe(game.host, "game_ready_up", ready=characters_ready_count, total=len(characters_playing)))
    if characters_ready_count == len(characters_playing):
        do_turn(bot_instance, game, characters_playing, communication_callback)


def do_turn(bot_instance, game, characters_playing, communication_callback):
    communication_callback(text=localization.format_safe(game.host, "game_turn", turn=game.turn))
    for character in characters_playing:
        game_action_dispatcher.process_action(character, communication_callback)

    characters_playing = models.PlayerCharacter.select().where(models.PlayerCharacter.game == game)
    active_floors = set()
    for character in characters_playing:
        active_floors.add(character.floor)

    for floor in active_floors:
        if not floor.visited:
            floor.visited = True
            floor.save()
        else:
            if enemy_turn(game, floor, communication_callback):
                communication_callback(flush=True)
                break
    else:
        game.turn += 1
        game.save()
        characters_playing = models.PlayerCharacter.select().where(models.PlayerCharacter.game == game)
        communication_callback(text=localization.format_safe(game.host, "game_planning", turn=game.turn), flush=True)
        for character in characters_playing:
            bot.create_control_message(bot_instance, character)


def enemy_turn(game, floor, communication_callback):
    entity_data = json.loads(floor.entity_data)

    if len(entity_data['enemies']) == 0:
        return

    characters = list(models.PlayerCharacter.select().where(models.PlayerCharacter.floor == floor))

    for enemy_id, enemy in list(entity_data['enemies'].items()):

        # DOT Status effects
        best_dot = {}
        stun = None
        for status_effect in list(enemy['status_effects']):
            if status_effect['type'] == 'stun':
                if stun is None:
                    stun = status_effect
                else:
                    if stun['duration'] < status_effect:
                        enemy['status_effects'].remove(stun)
                        stun = status_effect
                    else:
                        enemy['status_effects'].remove(status_effect)
                continue

            if status_effect['type'] not in ['fire', 'poison']:
                continue
            if status_effect['type'] not in best_dot or status_effect['damage'] > best_dot[status_effect['type']]['damage']:
                best_dot[status_effect['type']] = status_effect
                continue

        for status_effect in best_dot.values():
            communication_callback(text=localization.format_safe(
                game.host,
                f"battle_taking_damage_{status_effect['type']}",
                target=describe_enemy(game.host, enemy),
                damage=status_effect['damage']
            ))
            status_effect['duration'] -= 1
            if status_effect['duration'] <= 0:
                enemy['status_effects'].remove(status_effect)
                communication_callback(text=localization.format_safe(
                    game.host,
                    f"status_effect_{status_effect['type']}_end",
                    target=describe_enemy(game.host, enemy)
                ))

            damage_enemy(entity_data, game.host, enemy, enemy_id, status_effect['damage'], communication_callback)
            if enemy['health'] <= 0:
                break

        if enemy['health'] <= 0:
            continue

        if stun is not None:
            stun['duration'] -= 1
            communication_callback(text=localization.format_safe(
                game.host,
                f"status_effect_stun_active",
                target=describe_enemy(game.host, enemy)
            ))
            if stun['duration'] <= 0:
                enemy['status_effects'].remove(stun)
                communication_callback(text=localization.format_safe(
                    game.host,
                    f"status_effect_stun_end",
                    target=describe_enemy(game.host, enemy)
                ))
            continue

        target = random.choice(characters)
        communication_callback(text=localization.format_safe(game.host, "battle_attack", name=describe_enemy(game.host, enemy), target=target.name))

        combo = process_attack(game.host, communication_callback)

        if combo > 0:
            damage = enemy['ATK']*2**(combo-1)
            communication_callback(text=localization.format_safe(game.host, "battle_taking_damage", target=target.name, damage=damage))
            target.health -= damage

            target.save()
            if target.health <= 0:
                communication_callback(text=localization.format_safe(
                    game.host,
                    "battle_death",
                    name=target.name
                ))
                final_floor = target.floor.height
                target.delete_instance()
                if len(models.PlayerCharacter.select().where(models.PlayerCharacter.game == game)) == 0:
                    communication_callback(text=localization.format_safe(
                        game.host,
                        "play_game_over",
                        floor=final_floor,
                        seed=game.seed
                    ))
                    game.delete_instance()
                    return True

    floor.entity_data = json.dumps(entity_data)
    floor.save()
