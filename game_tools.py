import json
import localization
import items.dispatcher

with open("items.json", encoding="utf-8") as item_file:
    ITEM_DATA = json.load(item_file)


def add_data(func):
    def new_func(character, *args, **kwargs):
        inventory = json.loads(character.inventory_data)
        entities = json.loads(character.floor.entity_data)
        return func(inventory, entities, character, *args, **kwargs)
    return new_func


def is_in_battle(character):
    entity_data = json.loads(character.floor.entity_data)
    return len(entity_data["enemies"]) > 0


def process_attack(user, communication_callback):
    combo = 0
    while True:
        if combo == 0:
            text = None
        elif combo == 1:
            text = localization.localize(user, "battle_crit_2")
        elif combo == 2:
            text = localization.localize(user, "battle_crit_3")
        elif combo == 3:
            text = localization.localize(user, "battle_crit_4")
        else:
            text = localization.format_safe(user, "battle_crit_5+", combo=combo)

        roll = communication_callback(roll=True, text=text)

        if combo == 0:
            if roll == 1:
                communication_callback(text=localization.localize(user, "battle_miss"))
                return combo
        combo += 1
        if roll < 6:
            return combo


def register_item(entity_data, item):
    entity_data['last_item_id'] += 1
    entity_data['items'][entity_data['last_item_id']] = item


def register_enemy(entity_data, enemy):
    entity_data['last_enemy_id'] += 1
    entity_data['enemies'][entity_data['last_enemy_id']] = enemy


def register_object(entity_data, object):
    entity_data['last_object_id'] += 1
    entity_data['objects'][entity_data['last_object_id']] = object


def describe_inventory(user, character):
    character_inventory = json.loads(character.inventory_data)

    if character_inventory["weapon"] is None:
        weapon_description = localization.localize(user, "inventory_weapon_none")
    else:
        weapon_name = items.dispatcher.describe_item(user, character_inventory["weapon"])
        weapon_description = localization.format_safe(user, "inventory_weapon", weapon=weapon_name)

    if len(character_inventory["items"]) > 0:
        inventory_description = list()
        for item_id, item in enumerate(character_inventory["items"]):
            inventory_description.append(f"{item_id+1} \\- {items.dispatcher.describe_item(user, item)}")

        inventory_description = localization.localize(user, "inventory_nonempty") + "\n" + "\n".join(inventory_description)
    else:
        inventory_description = localization.localize(user, "inventory_empty")

    return f"{weapon_description}\n\n{inventory_description}\n\n{describe_character(user, character)}"


def describe_character(user, character):
    return localization.format_safe(
        user,
        "character_status",
        health=character.health,
        hp=character.HP,
        mana=character.mana,
        mp=character.MP
    )


def describe_spellbook(user, character):
    character_inventory = json.loads(character.inventory_data)

    if len(character_inventory["spells"]) > 0:
        spell_description = list()
        for spell_id, spell in enumerate(character_inventory["spells"]):
            spell_description.append(f"{spell_id+1} \\- {localization.localize(user, spell)}")

        spell_description = localization.localize(user, "spellbook_nonempty") + "\n" + "\n".join(spell_description)
    else:
        spell_description = localization.localize(user, "spellbook_empty")

    return f"{spell_description}\n\n{describe_character(user, character)}"


def describe_enemy(user, enemy):
    main_name = localization.localize(user, enemy["name"])
    main_name = main_name[0].upper() + main_name[1:]

    return f"{main_name} \\[{enemy['health']}/{enemy['HP']} HP \\| {enemy['ATK']} ATK\\]"


def describe_status_effect(user, effect):
    main_name = localization.localize(user, f"status_effect_{effect['type']}")

    additional_info = ""
    if effect['type'] == 'fire' or effect['type'] == 'poison':
        additional_info = f" \\[{effect['damage']} " + localization.localize(user, "damage") + "\\] "

    return f"{main_name}{additional_info} \\({effect['duration']} {localization.format_safe(user, 'turns')}\\)"


def describe_add_rarity(user, string, rarity):
    rarity_localized = localization.localize(user, rarity)
    md_before = ITEM_DATA["rarities"][rarity]["md_before"]
    md_after = ITEM_DATA["rarities"][rarity]["md_after"]
    rarity_localized = rarity_localized[0].upper() + rarity_localized[1:]
    return f"{md_before}{rarity_localized} {string}{md_after}"


def damage_enemy(entity_data, user, target, target_id, damage, communication_callback):
    oneshot = target["health"] == target["HP"]
    target["health"] -= damage
    if target["health"] <= 0:
        target["health"] = 0

    if target["health"] <= 0:
        if oneshot:
            communication_callback(text=localization.format_safe(user, "battle_death_oneshot",
                                                                 name=describe_enemy(user, target)))
        else:
            communication_callback(
                text=localization.format_safe(user, "battle_death", name=describe_enemy(user, target)))
        del entity_data['enemies'][target_id]
        if target["dropped_item"] is not None:
            communication_callback(
                text=localization.format_safe(user,
                                              "battle_drop",
                                              target=describe_enemy(user, target),
                                              item=items.dispatcher.describe_item(user, target['dropped_item'])
                                              ))
            register_item(entity_data, target['dropped_item'])


def apply_status_effect(entity_data, user, target, status_effect, communication_callback):
    if status_effect['duration'] <= 0:
        communication_callback(text=localization.format_safe(
            user,
            f"status_effect_{status_effect['type']}_fail",
            target=describe_enemy(user, target)
        ))
        return

    for old_effect in list(target['status_effects']):
        if old_effect['type'] == status_effect['type']:
            if old_effect['duration'] > status_effect['duration']:
                break
            else:
                target['status_effects'].remove(old_effect)
    else:
        target['status_effects'].append(status_effect)
        communication_callback(text=localization.format_safe(
            user,
            f"status_effect_{status_effect['type']}_begin",
            target=describe_enemy(user, target)
        ))
