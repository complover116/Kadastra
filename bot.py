import os
import version_manager

import models
import peewee

from bot_keyboards import get_inline_keyboard_okay, get_inline_keyboard_normal
from localization import localize, format_safe
import telegram.ext
import telegram
import game_logic
import time
import threading
import game_command_dispatcher
import random
assert "API_KEY" in os.environ

updater = telegram.ext.Updater(token=os.environ["API_KEY"], use_context=True)
dispatcher = updater.dispatcher

chat_locks = dict()

MAX_MESSAGES_PER_MINUTE_PER_CHAT = 28
MIN_MESSAGE_INTERVAL = 60/MAX_MESSAGES_PER_MINUTE_PER_CHAT

last_message_times = {}
message_bunchup_cache = {}


def rate_limit(chat_id):
    if chat_id in last_message_times:
        delay = last_message_times[chat_id] + MIN_MESSAGE_INTERVAL - time.time()
        if delay > 0:
            time.sleep(delay)
    last_message_times[chat_id] = time.time()


def run_in_thread(fn):
    def run(*k, **kw):
        t = threading.Thread(target=fn, args=k, kwargs=kw)
        t.start()
        return t
    return run


def acquire_chat_lock(chat_id):
    if chat_id not in chat_locks:
        chat_locks[chat_id] = threading.Lock()
    chat_locks[chat_id].acquire()


def release_chat_lock(chat_id):
    chat_locks[chat_id].release()


def get_user_from_telegram_user(telegram_user):
    user = models.User.get_or_create(telegram_user_id=telegram_user.id)[0]
    if user.telegram_user_name != telegram_user.username:
        user.telegram_user_name = telegram_user.username.replace("_", "\\_")
        user.save()
    return user


def get_user(update):
    return get_user_from_telegram_user(update.message.from_user)


def get_game(update):
    try:
        return models.Game.get(telegram_chat_id=update.effective_chat.id, active=True)
    except peewee.DoesNotExist:
        return None


def flush_bunchup_cache(bot, chat_id):
    if chat_id in message_bunchup_cache:
        message = "\n\n".join(message_bunchup_cache[chat_id])
        rate_limit(chat_id)
        bot.send_message(
            chat_id=chat_id,
            text=message,
            parse_mode="MarkdownV2"
        )
        message_bunchup_cache[chat_id].clear()


def queue_bunchup_message(chat_id, message):
    if chat_id not in message_bunchup_cache:
        message_bunchup_cache[chat_id] = list()

    message_bunchup_cache[chat_id].append(message)


def get_communication_callback(update, bot):
    def communication_callback(text=None, roll=False, reply_markup=None, flush=False):
        if text is not None:
            queue_bunchup_message(update.effective_chat.id, text)
        if roll:
            user = get_user_from_telegram_user(update.callback_query.from_user)
            if user.fast_roll:
                result = random.randrange(1, 7)
                queue_bunchup_message(update.effective_chat.id, f"🎲 *{result}*")
                return result
            else:
                flush_bunchup_cache(bot, update.effective_chat.id)
                rate_limit(update.effective_chat.id)
                reply = bot.send_dice(
                    chat_id=update.effective_chat.id
                )
                time.sleep(4.5)
                return reply.dice.value
        if flush:
            flush_bunchup_cache(bot, update.effective_chat.id)
    return communication_callback


def start(update, context):
    rate_limit(update.effective_chat.id)
    update.message.reply_markdown_v2(text=localize(get_user(update), "start"))


dispatcher.add_handler(telegram.ext.CommandHandler("start", start))


def help(update, context):
    rate_limit(update.effective_chat.id)
    update.message.reply_markdown_v2(text=localize(get_user(update), "help"))


dispatcher.add_handler(telegram.ext.CommandHandler("help", help))


def create_control_message(bot, character):
    default_keyboard = get_inline_keyboard_normal(character.user, character)
    chat_id = character.game.telegram_chat_id
    rate_limit(chat_id)
    character.telegram_control_message_id = bot.send_message(
        chat_id=chat_id,
        text=f"@{character.user.telegram_user_name}\n{game_logic.describe_floor(character.user, character)}",
        reply_markup=default_keyboard,
        parse_mode="MarkdownV2"
    ).message_id
    character.save()
    time.sleep(2)


def lang(update, context):
    rate_limit(update.effective_chat.id)
    user = get_user(update)
    if len(context.args) != 1:
        reply_markup = telegram.ReplyKeyboardMarkup([["/lang EN", "/lang RU"]], one_time_keyboard=True, resize_keyboard=True)
        update.message.reply_markdown_v2(text=localize(user, "lang"), reply_markup=reply_markup)
        return
    if context.args[0] not in ["EN", "RU"]:
        update.message.reply_markdown_v2(text=localize(user, "lang_unknown"))
        return

    user.language = context.args[0]
    user.save()

    update.message.reply_markdown_v2(text=localize(user, "lang_changed"))


dispatcher.add_handler(telegram.ext.CommandHandler("lang", lang))


def dice(update, context):
    rate_limit(update.effective_chat.id)
    user = get_user(update)
    if len(context.args) != 1:
        reply_markup = telegram.ReplyKeyboardMarkup([["/dice ON", "/dice OFF"]], one_time_keyboard=True, resize_keyboard=True)
        update.message.reply_markdown_v2(text=localize(user, "fast_roll"), reply_markup=reply_markup)
        return
    if context.args[0] not in ["ON", "OFF"]:
        update.message.reply_markdown_v2(text=localize(user, "fast_roll_unknown"))
        return

    enabled = context.args[0] == "OFF"
    user.fast_roll = enabled
    user.save()

    if enabled:
        update.message.reply_markdown_v2(text=localize(user, "fast_roll_on"))
    else:
        update.message.reply_markdown_v2(text=localize(user, "fast_roll_off"))


dispatcher.add_handler(telegram.ext.CommandHandler("dice", dice))


@run_in_thread
def play(update, context):
    rate_limit(update.effective_chat.id)
    user = get_user(update)
    game = get_game(update)
    if game is not None:
        if len(context.args) == 1 and context.args[0] == "reset":
            # game.active = False
            game.delete_instance()
            update.message.reply_markdown_v2(text=localize(user, "play_game_reset"))
            return
        else:
            if len(models.PlayerCharacter.select().where(models.PlayerCharacter.user == user, models.PlayerCharacter.game == game)) > 0:
                update.message.reply_markdown_v2(text=localize(user, "play_already_playing"))
                create_control_message(context.bot, character = models.PlayerCharacter.get(game=game, user=user))
                return

    if len(context.args) < 1:
        update.message.reply_markdown_v2(text=localize(user, "play_enter_name"))
        return

    name = context.args[0]

    if game is None:
        seed = None
        if len(context.args) == 2:
            try:
                seed = int(context.args[1])
            except ValueError:
                update.message.reply_markdown_v2(text=localize(user, "play_game_invalid_seed"))
                return
        game = game_logic.prepare_game(user, update.effective_chat.id, seed)

    update.message.reply_markdown_v2(text=format_safe(user, "play_game_joined", name=name))

    character = game_logic.join_game(user, game, name)


dispatcher.add_handler(telegram.ext.CommandHandler("play", play))


def seed(update, context):
    rate_limit(update.effective_chat.id)
    user = get_user(update)
    game = get_game(update)
    if game is None:
        update.message.reply_text(format_safe(user, "seed_no_game", version=version_manager.CURRENT_VERSION))
        return

    update.message.reply_text(format_safe(user, "seed", seed=game.seed, version=version_manager.CURRENT_VERSION))


dispatcher.add_handler(telegram.ext.CommandHandler("seed", seed))


def ready(update, context):
    # rate_limit(update.effective_chat.id)
    user = get_user(update)
    game = get_game(update)
    if game is None:
        update.message.reply_markdown_v2(text=localize(user, "play_no_game"))
        return
    try:
        character = models.PlayerCharacter.get(game=game, user=user)
    except peewee.DoesNotExist:
        update.message.reply_markdown_v2(text=localize(user, "ready_no_character"))
        return

    character.ready = True
    character.save()

    for character in models.PlayerCharacter.select().where(models.PlayerCharacter.game == game):
        if not character.ready:
            break
    else:
        game_logic.start_game(game)
        context.bot.send_message(chat_id=update.effective_chat.id, text=format_safe(game.host, "play_game_seed", seed=game.seed, version=version_manager.CURRENT_VERSION))
        context.bot.send_message(parse_mode="MarkdownV2", chat_id=update.effective_chat.id, text=localize(game.host, "play_game_started"))
        for character in models.PlayerCharacter.select().where(models.PlayerCharacter.game == game):
            create_control_message(bot=context.bot, character=character)


dispatcher.add_handler(telegram.ext.CommandHandler("ready", ready))


@run_in_thread
def handle_telegram_callback(update, context):
    query = update.callback_query
    if query.message.chat.id not in chat_locks:
        chat_locks[query.message.chat.id] = threading.Lock()

    with chat_locks[query.message.chat.id]:
        user = get_user_from_telegram_user(query.from_user)
        game = get_game(update)
        try:
            character = models.PlayerCharacter.get(game=game, user=user)
        except peewee.DoesNotExist:
            query.answer()
            return

        if character.telegram_control_message_id != query.message.message_id:
            query.answer()
            return

        if game is None:
            query.edit_message_text(localize(user, "play_game_lost"))
            query.edit_message_reply_markup(reply_markup=None)
            query.answer()
            return

        result = game_command_dispatcher.process_request(character, query, user, game)

        if type(result) is str:
            target_text = f"@{character.user.telegram_user_name}\n*{result}*"
            query.edit_message_text(
                text=target_text,
                parse_mode="MarkdownV2",
                reply_markup=get_inline_keyboard_okay(user, character)
            )
        query.answer()

        if result is True:
            game_logic.check_and_do_turn(context.bot, game, get_communication_callback(update, context.bot))


dispatcher.add_handler(telegram.ext.CallbackQueryHandler(handle_telegram_callback))
