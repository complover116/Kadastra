import game_tools
import localization
from . import constants

REQUIRED_TARGET = constants.TARGET_ENEMY


def plan_cast(inventory, entity_data, character, query, user, game, spell_power, target):
    return constants.PLAN_OK


def cast(inventory, entity_data, character, user, game, spell_power, mana_spent, target, target_id, communication_callback):
    hp_stealing = mana_spent*game_tools.ITEM_DATA["spells"]["spell_lifesteal"]["hp_multiplier"]
    hp_stealing = min(hp_stealing, target['health'])

    communication_callback(text=localization.format_safe(
        user,
        "spell_lifesteal_cast",
        name=character.name,
        target=game_tools.describe_enemy(user, target)
    ))

    game_tools.damage_enemy(entity_data, user, target, target_id, hp_stealing, communication_callback)

    character.health += hp_stealing
    if character.health > character.HP:
        character.health = character.HP

    communication_callback(text=localization.format_safe(
        user,
        "effect_restore_health",
        name=character.name,
        healing=hp_stealing,
        health=character.health,
        hp=character.HP
    ))
