import math

from . import constants
import spells.spell_healing
import spells.spell_lifesteal
import spells.spell_fireball
import spells.spell_poison_rain
import game_tools
import localization
import bot_keyboards
import json
import models

SPELL_TYPES = dict()
SPELL_TYPES['spell_healing'] = spells.spell_healing
SPELL_TYPES['spell_lifesteal'] = spells.spell_lifesteal
SPELL_TYPES['spell_fireball'] = spells.spell_fireball
SPELL_TYPES['spell_poison_rain'] = spells.spell_poison_rain


def plan_cast(inventory, entity_data, character, query, user, game, spell_name, spell_power, target_id):
    assert spell_name in SPELL_TYPES
    spell_type = SPELL_TYPES[spell_name]
    if spell_power is None:
        text = [localization.format_safe(user, "plan_cast_power_select", spell=localization.localize(user, spell_name)), "\n"]

        text.append(
            localization.format_safe(
                user,
                "plan_cast_power_select_weak" + ("" if character.mana * 10 >= character.MP else "_nomana"),
                spell=localization.localize(user, spell_name)
            )
        )
        text.append(
            localization.format_safe(
                user,
                "plan_cast_power_select_normal" + ("" if character.mana * 10 // 3 >= character.MP else "_nomana"),
                spell=localization.localize(user, spell_name)
            )
        )
        text.append(
            localization.format_safe(
                user,
                "plan_cast_power_select_strong" + ("" if character.mana * 2 >= character.MP else "_nomana"),
                spell=localization.localize(user, spell_name)
            )
        )
        query.edit_message_text(
            text="\n".join(text),
            parse_mode="MarkdownV2",
            reply_markup=bot_keyboards.get_inline_keyboard_spell_power(user, character, f"cast {spell_name}")
        )
        return None
    if spell_type.REQUIRED_TARGET is not None:
        if target_id is None:
            if spell_type.REQUIRED_TARGET == constants.TARGET_ENEMY:
                query.edit_message_text(
                    text=localization.format_safe(user, "plan_cast_target_select", spell=localization.localize(user, spell_name)),
                    parse_mode="MarkdownV2",
                    reply_markup=bot_keyboards.get_inline_keyboard_enemy_target(user, character, f"cast {spell_name} {spell_power}")
                )
                return None
            else:
                assert False  # Invalid target type

        target = _get_target_from_id(inventory, entity_data, spell_type.REQUIRED_TARGET, target_id)
    else:
        target = None

    result = spell_type.plan_cast(inventory, entity_data, character, query, user, game, spell_power, target)
    if result == constants.PLAN_OK:
        if target is not None:
            return localization.format_safe(user, "plan_cast_target", name=character.name,
                                            spell=localization.localize(user, spell_name), target=game_tools.describe_enemy(user, target))
        else:
            return localization.format_safe(user, "plan_cast", name=character.name, spell=localization.localize(game.host, spell_name))
    else:
        return False, result


def cast(inventory, entity_data, character, user, game, spell_name, spell_power, target_id, communication_callback):
    assert spell_name in SPELL_TYPES
    spell_type = SPELL_TYPES[spell_name]
    if spell_type.REQUIRED_TARGET is not None:
        assert target_id is not None
        target = _get_target_from_id(inventory, entity_data, spell_type.REQUIRED_TARGET, target_id)
        if target is None:
            communication_callback(text=localization.format_safe(user, "action_cast_no_target"))
        else:
            # TODO: Support describing any target
            communication_callback(
                text=localization.format_safe(user, "action_cast_target", name=character.name,
                                              spell=localization.localize(game.host, spell_name),
                                              target=game_tools.describe_enemy(game.host, target))
            )
    else:
        communication_callback(
            text=localization.format_safe(user, "action_cast", name=character.name,
                                          spell=localization.localize(game.host, spell_name))
        )
        target = None

    if spell_power == 3:
        mana_spent = math.ceil(character.MP/2)
    elif spell_power == 2:
        mana_spent = math.ceil(character.MP/10*3)
    else:
        mana_spent = math.ceil(character.MP/10)

    result = spell_type.cast(
        inventory,
        entity_data,
        character,
        user,
        game,
        spell_power,
        mana_spent,
        target,
        target_id,
        communication_callback
    )
    with models.db.atomic():
        character.mana -= mana_spent
        character.inventory_data = json.dumps(inventory)
        character.save()
        character.floor.entity_data = json.dumps(entity_data)
        character.floor.save()


def _get_target_from_id(inventory, entity_data, target_type, target_id):
    if target_type == constants.TARGET_ENEMY:
        return entity_data['enemies'][target_id]
    else:
        assert False  # Invalid target type
