import game_tools
import localization
from . import constants

REQUIRED_TARGET = None


def plan_cast(inventory, entity_data, character, query, user, game, spell_power, target):
    return constants.PLAN_OK


def cast(inventory, entity_data, character, user, game, spell_power, mana_spent, target, target_id, communication_callback):
    healing = mana_spent*game_tools.ITEM_DATA["spells"]["spell_healing"]["hp_multiplier"]
    character.health += healing
    if character.health > character.HP:
        character.health = character.HP

    communication_callback(text=localization.format_safe(
        user,
        "effect_restore_health",
        name=character.name,
        healing=healing,
        health=character.health,
        hp=character.HP
    ))
