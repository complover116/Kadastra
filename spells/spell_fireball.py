import random

import game_tools
import localization
from . import constants

REQUIRED_TARGET = None


def plan_cast(inventory, entity_data, character, query, user, game, spell_power, target):
    if len(entity_data['enemies']) == 0:
        return localization.localize(user, "action_attack_no_enemies")
    return constants.PLAN_OK


def cast(inventory, entity_data, character, user, game, spell_power, mana_spent, target, target_id, communication_callback):
    fire_damage = mana_spent*game_tools.ITEM_DATA["spells"]["spell_fireball"]["damage_multiplier"]

    communication_callback(text=localization.format_safe(
        user,
        "spell_fireball_cast",
        name=character.name,
        damage=fire_damage
    ))

    for enemy_id, enemy in list(entity_data['enemies'].items()):
        game_tools.apply_status_effect(
            entity_data,
            user,
            enemy,
            {
                "type": "fire",
                "damage": fire_damage,
                "duration": random.randrange(2, 5)
            },
            communication_callback
        )

