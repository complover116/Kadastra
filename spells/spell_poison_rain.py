import random

import game_tools
import localization
from . import constants

REQUIRED_TARGET = None


def plan_cast(inventory, entity_data, character, query, user, game, spell_power, target):
    if len(entity_data['enemies']) == 0:
        return localization.localize(user, "action_attack_no_enemies")
    return constants.PLAN_OK


def cast(inventory, entity_data, character, user, game, spell_power, mana_spent, target, target_id, communication_callback):
    poison_damage = mana_spent*game_tools.ITEM_DATA["spells"]["spell_poison_rain"]["damage_multiplier"]

    communication_callback(text=localization.format_safe(
        user,
        "spell_poison_rain_cast",
        name=character.name,
        damage=poison_damage
    ))

    for enemy_id, enemy in list(entity_data['enemies'].items()):
        game_tools.apply_status_effect(
            entity_data,
            user,
            enemy,
            {
                "type": "poison",
                "damage": poison_damage,
                "duration": random.randrange(4, 10)
            },
            communication_callback
        )

