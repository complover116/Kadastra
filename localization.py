import json

with open("lang.json", encoding="utf-8") as lang:
    _LOCALIZATION_DATA = json.load(lang)


def localize(user, string):
    selected_lang = user.language
    if string not in _LOCALIZATION_DATA or selected_lang not in _LOCALIZATION_DATA[string]:
        string_safe = string.replace('_', '\\_')
        return f"\\#{string_safe}\\_{selected_lang}"

    return _LOCALIZATION_DATA[string][selected_lang]


def format_safe(user, string, *args, **kwargs):
    string_localized = localize(user, string)
    return string_localized.format(*args, **kwargs)
