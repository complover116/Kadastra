from peewee import *
from playhouse.sqlite_ext import SqliteExtDatabase
import logging

db = SqliteExtDatabase(
    "database.db",
    timeout=30,
    pragmas=(
        ('cache_size', -1024 * 64),
        ('journal_mode', 'wal'),
        ('foreign_keys', 1),
    )
)


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    telegram_user_id = IntegerField()
    telegram_user_name = CharField(null=True)
    language = CharField(default="RU")
    fast_roll = BooleanField(default=False)


class Game(BaseModel):
    telegram_chat_id = IntegerField()
    turn = IntegerField(default=1)
    active = BooleanField(default=True)
    started = BooleanField(default=False)
    seed = IntegerField()
    host = ForeignKeyField(User, on_delete="CASCADE")


class Floor(BaseModel):
    height = IntegerField()
    game = ForeignKeyField(Game, on_delete="CASCADE")
    # JSON Object. Contains arrays of entities
    entity_data = CharField()
    visited = BooleanField(default=False)


class PlayerCharacter(BaseModel):
    name = CharField()
    user = ForeignKeyField(User, on_delete="CASCADE")
    game = ForeignKeyField(Game, on_delete="CASCADE")
    floor = ForeignKeyField(Floor, null=True, on_delete="CASCADE")
    HP = IntegerField(default=100)
    health = IntegerField(default=100)
    MP = IntegerField(default=10)
    mana = IntegerField(default=10)
    ready = BooleanField(default=False)
    # JSON Object. Has keys such as "weapon", "armor", etc, along with general-purpose "items" array
    inventory_data = CharField()
    telegram_control_message_id = IntegerField(null=True)
    planned_action = CharField(null=True)


logging.info("Connecting to database")
db.connect()
logging.info("Creating tables")
db.create_tables([PlayerCharacter, User, Game, Floor])
