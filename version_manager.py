import subprocess

try:
    with open("version") as version_file:
        CURRENT_VERSION = version_file.readline()
except FileNotFoundError:
    CURRENT_VERSION = subprocess.check_output(["git", "describe", "--dirty=-modified", "--broken", "--always", "--tags"]).decode("utf-8").strip()
