from random import Random
import json

import models
import items.dispatcher
import objects.dispatcher
import logging

import game_tools


def generate_game(game, player_count):
    logging.info(f"Generating world {game.seed} for user {game.host.telegram_user_name}")
    random_instance = Random(game.seed)

    floor_data = dict()

    logging.info(f"Pass 1: Spawning enemies and random items...")
    # PASS 1: Spawn enemies and random items
    for floor_id in range(1, 101):
        entity_data = {
            "items": {},
            "last_item_id": 0,
            "enemies": {},
            "last_enemy_id": 0,
            "objects": {},
            "last_object_id": 0
        }

        if floor_id != 1:
            for i in range(player_count):
                if random_instance.random() < game_tools.ITEM_DATA["generator"]["random_item_chance"]:
                    game_tools.register_item(entity_data, generate_random_item(
                        random_instance,
                        level=floor_id,
                        rarity_modifier=game_tools.ITEM_DATA["generator"]["random_item_rarity"]
                    ))

            if random_instance.random() < game_tools.ITEM_DATA["generator"]["enemy_chance"]:
                for i in range(player_count):
                    total_attack = 0
                    first = True
                    while first or random_instance.random() < game_tools.ITEM_DATA['generator']['additional_enemy_chance']:
                        first = False
                        enemy = generate_enemy(
                            random_instance,
                            floor_id
                        )
                        total_attack += enemy['ATK']
                        if total_attack > game_tools.ITEM_DATA['generator']['hard_cap_total_attack_per_player']*100*exponential_level_scale(floor_id):
                            break
                        game_tools.register_enemy(entity_data, enemy)
                        if total_attack > game_tools.ITEM_DATA['generator']['max_total_attack_per_player']*100*exponential_level_scale(floor_id):
                            break

        else:
            for i in range(player_count):
                game_tools.register_item(
                    entity_data,
                    generate_weapon(
                        random_instance,
                        "uncommon",
                        floor_id
                    )
                )
                game_tools.register_item(
                    entity_data,
                    items.dispatcher.generate_item(
                        random_instance,
                        "item_health_potion",
                        "uncommon",
                        floor_id
                    )
                )

        floor_data[floor_id] = entity_data

    logging.info(f"Pass 2: Spawning monoliths...")
    for i in range(1, 11):
        if i != 10:
            target_floor = random_instance.randrange(
                -game_tools.ITEM_DATA["generator"]["monolith_floor_fuzz"],
                game_tools.ITEM_DATA["generator"]["monolith_floor_fuzz"]+1
            ) + i*10
        else:
            target_floor = 100

        game_tools.register_object(
            floor_data[target_floor],
            objects.dispatcher.generate_object(
                random_instance,
                "MONOLITH",
                "",
                target_floor
            )
        )
        floor_data[target_floor]['enemies'].clear()

    logging.info(f"Pass 3: Spawning chests and keys...")
    # PASS 2: Spawn chests
    for floor_id in range(1, 101):
        for i in range(player_count):
            if random_instance.random() < game_tools.ITEM_DATA["generator"]["chest_chance"]:
                rarity = generate_item_rarity(random_instance, game_tools.ITEM_DATA["generator"]["chest_rarity"])
                chest, key = generate_chest_with_key(random_instance, rarity, floor_id)
                game_tools.register_object(floor_data[floor_id], chest)
                potential_enemies = list()
                spread_low = game_tools.ITEM_DATA["rarities"][chest['rarity']]['chest_key_spread_low']
                spread_high = game_tools.ITEM_DATA["rarities"][chest['rarity']]['chest_key_spread_high']
                for target_floor_id in range(floor_id - spread_low, floor_id + spread_high + 1):
                    if target_floor_id not in floor_data:
                        continue
                    potential_enemies.extend(floor_data[target_floor_id]['enemies'].values())

                if len(potential_enemies) == 0:
                    # No one to drop the key - I guess there won't be a key then, too bad
                    continue
                not_dropping_enemies = list(filter(lambda enemy: enemy['dropped_item'] is None, potential_enemies))

                # If not - override an existing drop, should happen rarely enough that it doesn't skew drop rates much
                if len(not_dropping_enemies) > 0:
                    potential_enemies = not_dropping_enemies

                target_enemy = random_instance.choice(potential_enemies)

                target_enemy["dropped_item"] = key

    logging.info(f"Pass 4: Spawning spell books...")
    spells = ["spell_healing", "spell_lifesteal", "spell_fireball", "spell_poison_rain"]

    random_instance.shuffle(spells)

    for i, spell in enumerate(spells):
        target_floor = random_instance.randrange(
            -game_tools.ITEM_DATA["generator"]["spell_book_floor_fuzz"],
            game_tools.ITEM_DATA["generator"]["spell_book_floor_fuzz"] + 1
        ) + (i+1) * 7

        game_tools.register_item(
            floor_data[target_floor],
            {
                "name": "item_spell_book",
                "spell": spell
            }
        )

    logging.info(f"FINALIZING: Writing to db...")
    for floor_id in range(1, 101):
        models.Floor.create(
            game=game,
            height=floor_id,
            entity_data=json.dumps(floor_data[floor_id])
        )
    logging.info(f"Done generating {game.seed}!")


def random_float_between(random_instance, value_min, value_max):
    return value_min + (value_max - value_min) * random_instance.random()


def generate_item_rarity(random_instance: Random, rarity_modifier):
    rarity = None
    rarity_name = None
    for current_rarity, data in game_tools.ITEM_DATA["rarities"].items():
        if random_instance.random() < data['chance'] * rarity_modifier:
            rarity = data
            rarity_name = current_rarity
            break

    return rarity_name


def exponential_level_scale(level):
    return 1000**((level-1)/99)


def generate_random_item(random_instance: Random, level, rarity_modifier=None, fixed_rarity=None, chance_list=None):
    if chance_list is None:
        chance_list = game_tools.ITEM_DATA["generator"]["random_item_types_chances"]
    choose_from = list()
    assert rarity_modifier is not None or fixed_rarity is not None
    if fixed_rarity is not None:
        rarity = fixed_rarity
    else:
        rarity = generate_item_rarity(random_instance, rarity_modifier)

    for item_type, type_rarity in chance_list.items():
        choose_from.extend([item_type] * type_rarity)
    item_type = random_instance.choice(choose_from)
    if item_type == "WEAPON":
        return generate_weapon(random_instance, rarity, level)
    return items.dispatcher.generate_item(random_instance, item_type, rarity, level)


def generate_weapon(random_instance: Random, rarity, level):
    base_damage = game_tools.ITEM_DATA["weapon"]["base_damage"]*exponential_level_scale(level)
    multiplier = random_float_between(
        random_instance,
        game_tools.ITEM_DATA['rarities'][rarity]['damage_mul_min'],
        game_tools.ITEM_DATA['rarities'][rarity]['damage_mul_max']
    )
    weapon = {
        "name": random_instance.choice(game_tools.ITEM_DATA["weapon_types"]),
        "rarity": rarity,
        "damage": int(base_damage*multiplier),
        "type": "WEAPON"
    }
    return weapon


def generate_chest_with_key(random_instance: Random, rarity, level):
    chest = objects.dispatcher.generate_object(random_instance, "CHEST", rarity, level)
    key = items.dispatcher.generate_item(random_instance, "item_key", rarity, level)
    return chest, key


def generate_enemy(random_instance: Random, level):
    base_hp = game_tools.ITEM_DATA["enemy"]["base_hp"]*exponential_level_scale(level)
    base_attack = game_tools.ITEM_DATA["enemy"]["base_attack"]*exponential_level_scale(level)

    choose_from = list()
    for enemy_type, enemy_type_data in game_tools.ITEM_DATA["enemy_types"].items():
        choose_from.extend([enemy_type]*enemy_type_data['rarity'])
    enemy_type = random_instance.choice(choose_from)
    enemy_type_data = game_tools.ITEM_DATA["enemy_types"][enemy_type]

    atk_fuzz = game_tools.ITEM_DATA["generator"]["atk_fuzz"]
    hp_fuzz = game_tools.ITEM_DATA["generator"]["hp_fuzz"]

    atk = int(enemy_type_data["atk_mul"]*base_attack*(1 + random_instance.random()*atk_fuzz - atk_fuzz*2))
    hp = int(enemy_type_data["hp_mul"]*base_hp*(1 + random_instance.random()*hp_fuzz - hp_fuzz*2))

    dropped_item = None
    # if random_instance.random() < ITEM_DATA["generator"]["base_item_drop_chance"]*100/enemy_type_data['rarity']:
    #     dropped_item = generate_random_item(
    #         random_instance,
    #         enemy_type_data["dropped_item_rarity"],
    #         level,
    #         chance_list=ITEM_DATA["generator"]["dropped_item_types_chances"]
    #     )

    enemy = {
        "name": enemy_type,
        "ATK": atk,
        "HP": hp,
        "health": hp,
        "dropped_item": dropped_item,
        "status_effects": list()
    }
    return enemy
