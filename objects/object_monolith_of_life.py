import game_tools
import objects.dispatcher
import generator
import localization
from . import constants
import bot_keyboards

REQUIRED_TARGET = None


def plan_interact(inventory, entity_data, character, query, user, game, object):
    return constants.PLAN_OK


def interact(inventory, entity_data, character, user, game, obj, communication_callback):
    difference = obj['hp_upgrade_target'] - character.HP
    if difference <= 0:
        communication_callback(text=localization.format_safe(user, "action_interact_monolith_nothing", name=character.name))
        return

    character.HP = obj['hp_upgrade_target']
    character.health += difference

    difference = obj['mana_upgrade_target'] - character.MP
    character.mana += difference
    character.MP = obj['mana_upgrade_target']
    communication_callback(text=localization.format_safe(user,
                                                         "action_interact_monolith",
                                                         name=character.name,
                                                         hp=obj['hp_upgrade_target'],
                                                         mana=obj['mana_upgrade_target']
                                                         ))


def describe(user, object):
    description = f"{localization.localize(user, object['name'])}"
    return description


def generate(random_instance, rarity, level):
    hp_upgrade_target = int(generator.exponential_level_scale(level)*100)
    mana_upgrade_target = int(generator.exponential_level_scale(level) * 10)
    return {
        "name": "object_monolith_of_life",
        "hp_upgrade_target": hp_upgrade_target,
        "mana_upgrade_target": mana_upgrade_target,
        "type": "MONOLITH"
    }
