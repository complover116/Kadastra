from . import constants
import objects.object_chest
import objects.object_monolith_of_life

import game_tools
import localization
import bot_keyboards
import json
import models

OBJECT_TYPES = dict()
OBJECT_TYPES['CHEST'] = objects.object_chest
OBJECT_TYPES['MONOLITH'] = objects.object_monolith_of_life


def plan_interact(inventory, entity_data, character, query, user, game, object_id, object):
    assert object['type'] in OBJECT_TYPES
    object_type = OBJECT_TYPES[object['type']]
    result = object_type.plan_interact(inventory, entity_data, character, query, user, game, object)
    if result == constants.PLAN_OK:
        return localization.format_safe(user, "plan_interact", name=character.name, object=describe_object(game.host, object))
    elif result == constants.PLAN_MENU_DISPLAYED:
        return None
    else:
        return False, result


def interact(inventory, entity_data, character, user, game, object, communication_callback):
    assert object['type'] in OBJECT_TYPES
    object_type = OBJECT_TYPES[object['type']]

    result = OBJECT_TYPES[object['type']].interact(
        inventory,
        entity_data,
        character,
        user,
        game,
        object,
        communication_callback
    )
    with models.db.atomic():
        character.inventory_data = json.dumps(inventory)
        character.save()
        character.floor.entity_data = json.dumps(entity_data)
        character.floor.save()


def describe_object(user, object):
    main_name = localization.localize(user, object["name"])

    assert object['type'] in OBJECT_TYPES
    object_type = OBJECT_TYPES[object['type']]
    return object_type.describe(user, object)


def generate_object(random_instance, object_type_name, rarity, level):
    assert object_type_name in OBJECT_TYPES
    object_type = OBJECT_TYPES[object_type_name]
    return object_type.generate(random_instance, rarity, level)
