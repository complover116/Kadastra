PLAN_OK = "OK"  # Tell the command dispatcher to confirm the action
PLAN_FAIL = "FAIL"  # Tell the command dispatcher to display the message as an error
TARGET_ENEMY = "ENEMY"  # Enemy target type
TARGET_FRIEND = "FRIEND"  # Friend target type
TARGET_ANY = "ANY"  # Can target both friends and enemies
PLAN_TARGET_ENEMY = "TARGET_ENEMY"  # Tell the command dispatcher to ask for an enemy target
PLAN_MENU_DISPLAYED = "MENU_DISPLAYED"
