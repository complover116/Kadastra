import game_tools
import objects.dispatcher
import generator
import localization
from . import constants

REQUIRED_TARGET = None


def plan_interact(inventory, entity_data, character, query, user, game, object):
    return localization.format_safe(user, "plan_interact_chest", chest=objects.dispatcher.describe_object(user, object))


def interact(inventory, entity_data, character, user, game, item, target, target_id, communication_callback):
    return


def describe(user, object):
    description = game_tools.describe_add_rarity(user, f"{localization.localize(user, object['name'])}", object['rarity'])
    return description


def generate(random_instance, rarity, level):
    return {
        "name": "object_chest",
        "rarity": rarity,
        "item": generator.generate_random_item(random_instance, level, fixed_rarity=rarity, chance_list=game_tools.ITEM_DATA["generator"]["chest_item_types_chances"]),
        "type": "CHEST"
    }
