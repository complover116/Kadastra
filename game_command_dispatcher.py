import json
import game_logic
import game_tools
import localization
import logging
import bot_keyboards
import items.dispatcher
import objects.dispatcher
import spells.dispatcher


DISPATCHER_DICT = dict()


def process_request(character, query, user, game):
    logging.info(f"Received command {query.data} from user {user.telegram_user_name}, acting on behalf of character {character.name}")

    command = query.data.split()[0]
    if command not in DISPATCHER_DICT:
        query.edit_message_text(text=f"GAME DISPATCHER ERROR: invalid command {command}")
        return

    result = DISPATCHER_DICT[command](character, query, user, game)

    if type(result) is str:
        logging.info(f"Action confirmed for character {character.name}: {query.data}")
        query.edit_message_text(text=result, reply_markup=None, parse_mode="MarkdownV2")
        character.planned_action = query.data
        character.save()
        return True
    elif type(result) is None:
        pass # Message edit already handled
        return None
    elif type(result) is tuple:
        return result[1]


@game_tools.add_data
def take(inventory, entities, character, query, user, game):
    command = query.data.split()
    if len(command) == 1:
        query.edit_message_text(
            text=localization.localize(user, "plan_take_target"),
            parse_mode="MarkdownV2",
            reply_markup=bot_keyboards.get_inline_keyboard_take(user, character)
        )
        return None

    item_id = command[1]
    item = entities['items'][item_id]
    if "type" not in item and len(inventory['items']) >= 5:
        return False, localization.format_safe(user, "inventory_no_free_slots", name=character.name)
    return localization.format_safe(user, "plan_take", name=character.name, item=items.dispatcher.describe_item(user, item))


DISPATCHER_DICT["take"] = take


@game_tools.add_data
def attack(inventory, entities, character, query, user, game):
    if len(entities['enemies']) == 0:
        return False, localization.localize(user, "action_attack_no_enemies")

    if inventory["weapon"] is None:
        return False, localization.localize(user, "action_attack_no_weapon")

    command = query.data.split()
    if len(command) == 2:
        target = entities['enemies'][query.data.split()[1]]
    else:
        if len(entities['enemies']) == 1:
            target = list(entities['enemies'].values())[0]
        else:
            query.edit_message_text(
                text=localization.localize(user, "plan_attack_target"),
                parse_mode="MarkdownV2",
                reply_markup=bot_keyboards.get_inline_keyboard_enemy_target(user, character, "attack")
            )
            return None
    return localization.format_safe(user, "plan_attack", name=character.name, target=game_tools.describe_enemy(user, target))


DISPATCHER_DICT["attack"] = attack


@game_tools.add_data
def ascend(inventory, entities, character, query, user, game):
    return localization.format_safe(user, "plan_ascend", name=character.name)


DISPATCHER_DICT["ascend"] = ascend


def describe(character, query, user, game):
    query.edit_message_text(
        text=f"@{character.user.telegram_user_name}\n{game_logic.describe_floor(character.user, character)}",
        parse_mode="MarkdownV2",
        reply_markup=bot_keyboards.get_inline_keyboard_normal(user, character)
    )
    return


DISPATCHER_DICT["describe"] = describe


def inventory(character, query, user, game):
    query.edit_message_text(
        text=game_tools.describe_inventory(user, character),
        parse_mode="MarkdownV2",
        reply_markup=bot_keyboards.get_inline_keyboard_inventory(user, character)
    )
    return


DISPATCHER_DICT["inventory"] = inventory


def spellbook(character, query, user, game):
    query.edit_message_text(
        text=game_tools.describe_spellbook(user, character),
        parse_mode="MarkdownV2",
        reply_markup=bot_keyboards.get_inline_keyboard_cast(user, character)
    )
    return


DISPATCHER_DICT["spellbook"] = spellbook


@game_tools.add_data
def descend(inventory, entities, character, query, user, game):
    if game_tools.is_in_battle(character):
        return localization.format_safe(user, "plan_descend_run", name=character.name)
    else:
        return localization.format_safe(user, "plan_descend", name=character.name)


DISPATCHER_DICT["descend"] = descend


@game_tools.add_data
def use(inventory, entities, character, query, user, game):
    command = query.data.split()
    item_id = int(command[1])
    target_id = None
    if len(command) == 3:
        target_id = command[2]
    return items.dispatcher.plan_use(inventory, entities, character, query, user, game, item_id, inventory['items'][item_id], target_id)


DISPATCHER_DICT["use"] = use


@game_tools.add_data
def cast(inventory, entities, character, query, user, game):
    command = query.data.split()
    spell_name = command[1]
    target_id = None
    spell_power = None

    if len(command) >= 3:
        spell_power = int(command[2])
    if len(command) == 4:
        target_id = command[3]
    return spells.dispatcher.plan_cast(inventory, entities, character, query, user, game, spell_name, spell_power, target_id)


DISPATCHER_DICT["cast"] = cast


@game_tools.add_data
def interact(inventory, entities, character, query, user, game):
    command = query.data.split()
    object_id = command[1]
    return objects.dispatcher.plan_interact(inventory, entities, character, query, user, game, object_id, entities['objects'][object_id])


DISPATCHER_DICT["interact"] = interact


@game_tools.add_data
def drop(inventory, entities, character, query, user, game):
    item_id = int(query.data.split()[1])
    return localization.format_safe(user, "plan_drop", name=character.name, item=items.dispatcher.describe_item(user, inventory['items'][item_id]))


DISPATCHER_DICT["drop"] = drop


def idle(character, query, user, game):
    return localization.format_safe(user, "plan_idle", name=character.name)


DISPATCHER_DICT["idle"] = idle
