#!/usr/local/bin/python3
import subprocess
import sys
import os
import time


UPDATE_DELAY_MARGIN = 10
MAX_RESTARTS = 3
restarts = 0


print("Manager process started")
while True:
    with open("/home/kadastra-ci/app/version") as version_file:
        CURRENT_VERSION = version_file.readline()

    print(f"Current Kadastra version is {CURRENT_VERSION}")

    if os.path.exists("/home/kadastra-ci/venv_new"):
        print("Updating venv")
        subprocess.run(["rm", "-rf", "/home/kadastra-ci/venv"])
        subprocess.run(["mv", "/home/kadastra-ci/venv_new", "/home/kadastra-ci/venv"])
    if os.path.exists("/home/kadastra-ci/app_new"):
        with open("/home/kadastra-ci/app_new/version") as version_file:
            NEW_VERSION = version_file.readline()
        print(f"New version detected: {NEW_VERSION}")
        if NEW_VERSION.split(".")[0] != CURRENT_VERSION.split(".")[0]:
            print("Updating app, wiping database")
        else:
            print("Updating app, saving database...")
            os.rename("/home/kadastra-ci/app/database.db", "/home/kadastra-ci/database_saved_tmp.db")
            if os.path.exists("/home/kadastra-ci/app/database.db-wal"):
                os.rename("/home/kadastra-ci/app/database.db-wal", "/home/kadastra-ci/database_saved_tmp.db-wal")
        subprocess.run(["rm", "-rf", "/home/kadastra-ci/app"])
        subprocess.run(["mv", "/home/kadastra-ci/app_new", "/home/kadastra-ci/app"])
        if NEW_VERSION.split(".")[0] == CURRENT_VERSION.split(".")[0]:
            print("Restoring database...")
            os.rename("/home/kadastra-ci/database_saved_tmp.db", "/home/kadastra-ci/app/database.db")
            if os.path.exists("/home/kadastra-ci/database_saved_tmp.db-wal"):
                os.rename("/home/kadastra-ci/database_saved_tmp.db-wal", "/home/kadastra-ci/app/database.db-wal")

        CURRENT_VERSION = NEW_VERSION

    print("Starting Kadastra")
    kadastra = subprocess.Popen(['/home/kadastra-ci/venv/bin/python3', 'main.py'], cwd='/home/kadastra-ci/app/')

    while True:
        if kadastra.poll() is not None:
            print("Crash detected!")
            restarts += 1
            if restarts > MAX_RESTARTS:
                sys.exit(-1)
            break
        if os.path.exists("/home/kadastra-ci/restart"):
            try:
                with open("/home/kadastra-ci/restart") as restart_time_file:
                    restart_time = int(restart_time_file.readline())
            except ValueError:
                restart_time = time.time() + 3
            print(f"Restart requested in {int(restart_time - time.time())}s, waiting")
            time.sleep(int(restart_time - time.time()))
            print("Waiting extra time for Kadastra to shutdown")
            time.sleep(UPDATE_DELAY_MARGIN)
            print("Terminating")
            kadastra.terminate()
            os.remove("/home/kadastra-ci/restart")
            print("Waiting for bot polls to finish")
            time.sleep(10)
            break
        time.sleep(2)
