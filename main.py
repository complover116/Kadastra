import logging
import signal
import sys

import version_manager
import os
import time

from telegram.error import Unauthorized

logging.basicConfig(level=logging.INFO,
                    format='[%(asctime)s-%(name)s/%(levelname)s] %(message)s')

import models
import bot
import localization

RESTART_FILE = "../restart"
NEW_VERSION_FILE = "../app_new/version"

logging.info(f"Running Kadastra {version_manager.CURRENT_VERSION}")


def simple_format_time(user, time):
    if time > 60 * 60:
        return str(int(time / 60 / 60)) + localization.format_safe(user, "hours")
    elif time > 60:
        return str(int(time / 60)) + localization.format_safe(user, "minutes")
    else:
        return str(time) + localization.format_safe(user, "seconds")


# TODO: Move this to version_manager.py
@bot.run_in_thread
def poll_for_updates():
    logging.info("Update checking thread active")
    while True:
        if os.path.exists(RESTART_FILE):
            with open(RESTART_FILE) as restart_file:
                restart_time = int(restart_file.readline())
            if os.path.exists(NEW_VERSION_FILE):
                with open(NEW_VERSION_FILE) as new_version_file:
                    new_version = new_version_file.readline().strip()
                if new_version.split(".")[0] == version_manager.CURRENT_VERSION.split(".")[0]:
                    message = "restart_update_minor"
                else:
                    message = "restart_update"
                logging.warning(f"Update to {new_version} scheduled in {restart_time - time.time()} seconds")
            else:
                new_version = version_manager.CURRENT_VERSION
                message = "restart"
                logging.warning(f"Restart scheduled in {int(restart_time - time.time())} seconds")

            games_active = models.Game.select().where(models.Game.active == True)

            if restart_time - time.time() > 1:
                for game in games_active:
                    try:
                        bot.updater.bot.send_message(
                            chat_id=game.telegram_chat_id,
                            text=localization.format_safe(
                                game.host,
                                message,
                                time=simple_format_time(game.host, restart_time - time.time()),
                                new_version=new_version.replace(".", "\\.").replace("-", "\\-"),
                                current_version=version_manager.CURRENT_VERSION.replace(".", "\\.").replace("-", "\\-")
                            ),
                            parse_mode="MarkdownV2"
                        )
                    except Unauthorized:
                        logging.warning(f"Blocked by {game.telegram_chat_id}, not sending update message")

                time.sleep(restart_time - time.time())

            for game in games_active:
                bot.updater.bot.send_message(
                    chat_id=game.telegram_chat_id,
                    text=localization.format_safe(
                        game.host,
                        "restart_now"
                    ),
                    parse_mode="MarkdownV2"
                )

            bot.updater.stop()
            logging.warning(f"Shutting down for restart now")
            break

        time.sleep(5)


def handle_termination(arg1, arg2):
    for game in models.Game.select().where(models.Game.active == True):
        bot.updater.bot.send_message(
            chat_id=game.telegram_chat_id,
            text=localization.format_safe(
                game.host,
                "restart_now"
            ),
            parse_mode="MarkdownV2"
        )

        bot.updater.stop()
        logging.warning(f"Shutting down for restart now")
        time.sleep(2)
        sys.exit(0)


signal.signal(signal.SIGTERM, handle_termination)

logging.info("Starting the Telegram bot")
bot.updater.start_polling()
logging.info("Starting update checking thread")
poll_for_updates()
logging.info("Now live")
# bot.updater.idle()
