FROM python:3.9.6-buster
RUN mkdir /app
WORKDIR /app
ADD . /app
RUN python3 -m venv /app/venv
RUN /app/venv/bin/pip3 install -r /app/requirements.txt
CMD /app/venv/bin/python3 main.py
